#internal network address for the database
#this output of the resource will be visible in console for making debugging easy
output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}
