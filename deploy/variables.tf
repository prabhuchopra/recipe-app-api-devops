variable "prefix" {
  type        = string
  default     = "raad"
  description = "our application name"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "prabhuChopra"
}

variable "db_username" {
  description = "username for the rds postgres instance"
}

variable "db_password" {
  description = "password for the rds postgres instance"
}

variable "bastian_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for api"
  default     = "903195818201.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "903195818201.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
