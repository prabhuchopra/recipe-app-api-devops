terraform {
  backend "s3" {
    bucket                  = "recipe-app-api-devops-tf"
    key                     = "recipe-app.tfstate"
    region                  = "eu-central-1"
    encrypt                 = true
    dynamodb_table          = "recipe-app-api-devops-tf-state-lock"
    shared_credentials_file = "~/.aws/credentials"
    # access_key = "AKIA5ESVKPDM3LBVRAEO"
    # secret_key = "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"
  }
}

provider "aws" {
  region                  = "eu-central-1"
  version                 = "~> 2.54.0"
  shared_credentials_file = "~/.aws/credentials"
}

#locals are ways that you can create dynamic variables inside tf
#interpolation syntax => concatenate strings from diff resources and variables within tf

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }

}

data "aws_region" "current" {

}